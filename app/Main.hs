{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Codegen
import qualified Data.Text as T
import Parser
import PrettyPrinting
import Renamer
import System.Environment
import System.FilePath
import TypeChecker
import Utils

typeCheckString :: String -> IO ()
typeCheckString s = case runTypechecker $ runRename $ parseProgram s of
  Right _ -> putStrLn "OK"
  Left e -> putStrLn $ T.unpack $ showError e

prettyPrintString :: Bool -> String -> IO ()
prettyPrintString rename = putStrLn . T.unpack . pretty
  where
    pretty =
      if rename
        then runPP . runRename . parseProgram
        else runPP . parseProgram

compileString :: String -> IO ()
compileString filename =
  writeIl
    . runTypechecker
    . runRename
    . parseProgram
    =<< readFile filename
  where
    writeIl = \case
      Right prgm ->
        let prgmName = takeBaseName filename
            dest = prgmName <.> "cil"
            ilSrc = compileProgram prgm
         in do
              writeFile dest $ T.unpack ilSrc
              putStrLn $ "Compiled " <> dest <> " successfully."
      Left e -> putStrLn $ T.unpack $ showError e

main :: IO ()
main = do
  args <- getArgs
  case args of
    [filename] -> compileString filename
    ["-p", filename] -> prettyPrintString False =<< readFile filename
    ["-r", filename] -> prettyPrintString True =<< readFile filename
    ["-t", filename] -> typeCheckString =<< readFile filename
    _ -> error "Unknown args format"
