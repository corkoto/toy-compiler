# Flake layout based on https://magnus.therning.org/2022-03-13-simple-nix-flake-for-haskell-development.html
{
  description = "Toy compiler";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      with nixpkgs.legacyPackages.${system};
      let
        project = devTools:
          pkgs.haskellPackages.developPackage {
            root = ./.;
            returnShellEnv = !(devTools == []);
            modifier = drv:
              pkgs.haskell.lib.addBuildTools drv (with pkgs.haskellPackages;
                [
                  alex
                  happy
                  mono
                ] ++ devTools);
          };
      in {
        packages.pkg = project [];

        defaultPackage = self.packages.${system}.pkg;

        devShell = project (with haskellPackages; [
          cabal-install
          hpack
          haskell-language-server
          hlint
          ormolu
        ]);
      });
}
