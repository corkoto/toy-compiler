{-# LANGUAGE OverloadedStrings #-}

module TypeCheckerSpec where

import Ast
import Data.Either (isRight)
import Parser
import Renamer
import Test.Hspec
import TestUtils
import TypeChecker
import Utils

runString :: String -> Either SrcError (Program TypeChecked TcAnn)
runString = runTypechecker . runRename . parseProgram

shouldPassTypeCheck :: String -> SpecWith ()
shouldPassTypeCheck file =
  it ("succeeds when ran on " <> file) $ do
    contents <- readFile path
    runString contents `shouldSatisfy` isRight
  where
    path = addTestPath file

shouldFailTypeCheckWith :: String -> SrcError -> SpecWith ()
shouldFailTypeCheckWith file expectedError =
  it ("returns error when ran on " <> file) $ do
    contents <- readFile path
    runString contents `shouldBe` Left expectedError
  where
    path = addTestPath file

spec :: Spec
spec = describe "runTypechecker" $ do
  shouldPassTypeCheck "conditional.c"
  "undeclared-variable-due-to-scope.c"
    `shouldFailTypeCheckWith` UndeclaredVariableAssignment "y" (6, 5)
  "arithmetic-on-bool.c"
    `shouldFailTypeCheckWith` ExpectedType (Prim Int) (Prim Bool) (4, 13)
  shouldPassTypeCheck "multiple-calls.c"
  shouldPassTypeCheck "pass-by-value.c"
  shouldPassTypeCheck "return-terminates-early.c"
  shouldPassTypeCheck "while-loop.c"
  shouldPassTypeCheck "recursive-fibonacci.c"
  shouldPassTypeCheck "absolute-value.c"
  shouldPassTypeCheck "booleans.c"
  shouldPassTypeCheck "if-in-loop.c"
  shouldPassTypeCheck "nested-conditionals.c"
  shouldPassTypeCheck "arithmetic.c"
  shouldPassTypeCheck "operators.c"
  shouldPassTypeCheck "mutual-recursion.c"
  shouldPassTypeCheck "nested-while.c"
  "int-function-no-return.c"
    `shouldFailTypeCheckWith` NoReturnStatement "f" (Prim Int) (6, 5)
  "print-undeclared-variable.c"
    `shouldFailTypeCheckWith` UndeclaredVariableUsage "x" (3, 11)
  shouldPassTypeCheck "assignments.c"
  shouldPassTypeCheck "funcall.c"
  shouldPassTypeCheck "if-scoping.c"
  "called-with-too-many-args.c"
    `shouldFailTypeCheckWith` CalledWithWrongArgCount "f" 1 2 (5, 13)
  "wrong-return-type.c"
    `shouldFailTypeCheckWith` ReturnTypeNotMatching (Prim Bool) (Prim Int) (8, 5)
  shouldPassTypeCheck "unary.c"
  shouldPassTypeCheck "shadowing.c"
  "repeat-parameter-name.c"
    `shouldFailTypeCheckWith` RepeatingParamName
      "repeating_parameter"
      "num"
      (5, 29)
  "duplicate-decl.c"
    `shouldFailTypeCheckWith` VariableAlreadyDeclared "x" (7, 9)
  shouldPassTypeCheck "for-loop.c"
  shouldPassTypeCheck "array-sum.c"
  shouldPassTypeCheck "array-indexing.c"
  shouldPassTypeCheck "array-return.c"
