void main()
{
    int c = 1;
    int i = 0;
    int n = 10;

    while (c <= n) {
        print(fibonacci(i));
        i = i + 1;
        c = c + 1;
    }

    return;
}

int fibonacci(int n)
{
    if (n == 0)
        return 0;
    else if (n == 1)
        return 1;
    else
        return fibonacci(n-1) + fibonacci(n-2);
}
