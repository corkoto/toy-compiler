int main()
{
    int sum = sum(10);
    print(sum);
    bool even = even(101);
    print(even);
    printrec(4);
    return 1;
}

int sum(int n)
{
    if (n > 1) {
	return n + sum(n - 1);
    } else {
	return 1;
    }
}

bool even(int n)
{
    return first(n);
}

bool first(int n){
    if (n > 1) {
	return second(n-1);
    } else {
	return false;
    }
}

bool second(int n){
    if (n > 1) {
	return first(n-1);
    } else {
	return true;
    }
}

void printrec(int n){
    if (n > 1) {
	print(n);
	printrec(n-1);
    } else {
	print(1);
    }
}
