void main()
{
    print(mkIntArr(100, 5)[95]);
    print(mkBoolArr(100, true)[95]);
}

int[] mkIntArr(int size, int defVal)
{
    int[] arr = new int[size];
    int i = 0;
    for (i = 0; i < size; i++)
        arr[i] = defVal;
    return arr;
}

bool[] mkBoolArr(int size, bool defVal)
{
    bool[] arr = new bool[size];
    int i = 0;
    for (i = 0; i < size; i++)
        arr[i] = defVal;
    return arr;
}
