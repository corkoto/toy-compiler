int main()
{
    int y = -5;
    while (y <= 10) {
        print(passthrough(y));
        y = y + 1;
    }
    return 1;
}

int passthrough(int x)
{
    return abs(x);
}

int abs(int x)
{
    if (x < 0)
	return -x;
    else
	return x;
}
