void f(int x_1, int x)
{
    print(x);
    int x = 5;
    {
        int x = 10;
        {
            int x = 15;
            print(x);
        }
        print(x);
    }
    print(x);
    print(x_1);
}

void main()
{
    f(1, 2);
}
