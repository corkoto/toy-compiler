int main()
{
    bool a = true;
    bool b = true;
    bool c = false;
    bool d = false;

    a = a && c || b;
    b = b || c && true;
    c = c || false;
    d = true && false;

    print(a);
    print(b);
    print(c);
    print(d);
    print(invert(a));
    print(invert(b));
    print(invert(c));
    print(invert(d));
    return 1;
}

bool invert(bool value)
{
    return !value;
}
