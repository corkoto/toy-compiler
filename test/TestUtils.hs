module TestUtils where

import Control.Monad
import System.FilePath
import Test.Hspec

addTestPath :: FilePath -> FilePath
addTestPath file = "test" </> "source-files" </> file

expectedOutputLines :: [(String, [String])]
expectedOutputLines =
  [ ("conditional.c", ["10"]),
    ("multiple-calls.c", ["4"]),
    ("pass-by-value.c", ["1"]),
    ("return-terminates-early.c", ["True", "True"]),
    ( "while-loop.c",
      [ "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19"
      ]
    ),
    ( "recursive-fibonacci.c",
      [ "0",
        "1",
        "1",
        "2",
        "3",
        "5",
        "8",
        "13",
        "21",
        "34"
      ]
    ),
    ( "absolute-value.c",
      [ "5",
        "4",
        "3",
        "2",
        "1",
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10"
      ]
    ),
    ( "booleans.c",
      [ "True",
        "True",
        "False",
        "False",
        "False",
        "False",
        "True",
        "True"
      ]
    ),
    ("if-in-loop.c", ["1", "4", "5", "9", "10"]),
    ( "nested-conditionals.c",
      [ "111",
        "121",
        "131",
        "141",
        "151",
        "161",
        "171",
        "181",
        "191",
        "201",
        "211",
        "221"
      ]
    ),
    ("arithmetic.c", ["1", "2", "3", "-2", "3", "10", "1", "-97", "8836"]),
    ("operators.c", ["True", "False", "True"]),
    ("mutual-recursion.c", ["55", "False", "4", "3", "2", "1"]),
    ( "nested-while.c",
      [ "0",
        "0",
        "0",
        "1",
        "0",
        "2",
        "0",
        "3",
        "1",
        "0",
        "1",
        "1",
        "1",
        "2",
        "1",
        "3",
        "2",
        "0",
        "2",
        "1",
        "2",
        "2",
        "2",
        "3",
        "3",
        "0",
        "3",
        "1",
        "3",
        "2",
        "3",
        "3"
      ]
    ),
    ("assignments.c", ["10", "10"]),
    ("funcall.c", ["1", "False", "3"]),
    ("if-scoping.c", ["50"]),
    ("unary.c", ["10", "True"]),
    ("shadowing.c", ["2", "15", "10", "5", "1"]),
    ("dec-and-inc.c", ["5", "6", "5", "5"]),
    ("for-loop.c", ["2", "3", "4", "5"]),
    ("array-indexing.c", ["0", "50"]),
    ("array-sum.c", ["20"]),
    ("array-return.c", ["5", "True"]),
    ("string.c", ["Hello \"world\"!"])
  ]

outputsLinesCorrectlyWhenRan :: (String -> IO [String]) -> SpecWith ()
outputsLinesCorrectlyWhenRan runner = forM_ expectedOutputLines go
  where
    go (file, output) =
      it ("prints expected output when ran on " <> file) $
        runner (addTestPath file) `shouldReturn` output
