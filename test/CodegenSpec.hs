{-# LANGUAGE OverloadedStrings #-}

module CodegenSpec where

import Codegen (compileProgram)
import Data.Either.Combinators
import qualified Data.Text as T
import Parser
import Renamer
import System.IO
import System.IO.Temp
import System.Process
import Test.Hspec
import TestUtils
import TypeChecker

monoCompileRunner :: String -> IO [String]
monoCompileRunner srcFile =
  withSystemTempFile "codegen.cil" $ \cilFile cilH ->
    withSystemTempFile "codegen.exe" $ \exeFile _ -> do
      cilContents <-
        compileProgram
          . fromRight'
          . runTypechecker
          . runRename
          . parseProgram
          <$> readFile srcFile
      hPutStr cilH $ T.unpack cilContents
      hFlush cilH
      (_, _, _, p1) <-
        createProcess $
          proc "ilasm" [cilFile, "/output:" <> exeFile]
      _ <- waitForProcess p1
      (_, Just hOut, _, p2) <-
        createProcess $
          (proc "mono" [exeFile]) {std_out = CreatePipe}
      _ <- waitForProcess p2
      lines <$> hGetContents hOut

spec :: Spec
spec =
  describe "compileProgram" $
    outputsLinesCorrectlyWhenRan monoCompileRunner
