{
{-# LANGUAGE OverloadedStrings #-}
module Lexer where

import Prelude hiding (lex)
import Data.Text (Text)
import qualified Data.Text as T
}

%wrapper "posn"
@integer = [0-9]+
@string = \".*\"
@identifier = [a-zA-Z_][a-zA-Z0-9_]*
@comment = "//".*\n

tokens :-
  $white+ ;
  @comment ;

  if { lex $ TokenKeyw KeywIf }
  else { lex $ TokenKeyw KeywElse }
  while { lex $ TokenKeyw KeywWhile }
  for { lex $ TokenKeyw KeywFor }
  return { lex $ TokenKeyw KeywReturn }
  int { lex $ TokenKeyw KeywInt }
  bool { lex $ TokenKeyw KeywBool }
  string { lex $ TokenKeyw KeywString }
  void { lex $ TokenKeyw KeywVoid }
  new { lex $ TokenKeyw KeywNew }

  true { lex $ TokenBool Ttrue }
  false { lex $ TokenBool Tfalse }

  \( { lex $ TokenSep SepLpar }
  \) { lex $ TokenSep SepRpar }
  \{ { lex $ TokenSep SepLbrace }
  \} { lex $ TokenSep SepRbrace }
  \[ { lex $ TokenSep SepLbracket }
  \] { lex $ TokenSep SepRbracket }
  \; { lex $ TokenSep SepSemi }
  \, { lex $ TokenSep SepComma }

  \= { lex $ TokenOp OpAsn }
  \|\| { lex $ TokenOp OpOr }
  \&& { lex $ TokenOp OpAnd }
  \== { lex $ TokenOp OpEq }
  \!= { lex $ TokenOp OpNeq }
  \< { lex $ TokenOp OpLt }
  \> { lex $ TokenOp OpGt }
  \<= { lex $ TokenOp OpLte }
  \>= { lex $ TokenOp OpGte }
  \+ { lex $ TokenOp OpPlus }
  \- { lex $ TokenOp OpMinus }
  \* { lex $ TokenOp OpMul }
  \/ { lex $ TokenOp OpDiv }
  \! { lex $ TokenOp OpNot }
  \++ { lex $ TokenOp OpInc }
  \-\- { lex $ TokenOp OpDec }

  @identifier { lexStr (TokenId . T.pack) }

  @integer { lexStr (TokenInt . T.pack) }

  @string { lexStr (TokenStr . T.pack) }
{
type Pos = (Int, Int)

extractPosn (AlexPn _ line col) = (line, col)

-- Partially apply these to create lexing functions that fit the type
-- expected by the Alex posn wrapper (AlexPosn -> String -> a)
lex :: Token -> AlexPosn -> String -> (Token, Pos)
lex tok pos _ = (tok, extractPosn pos)

lexStr :: (String -> Token) -> AlexPosn -> String -> (Token, Pos)
lexStr f pos str = (f str, extractPosn pos)

data Keyw = KeywIf
          | KeywElse
          | KeywWhile
          | KeywFor
          | KeywReturn
          | KeywInt
          | KeywBool
          | KeywString
          | KeywVoid
          | KeywNew
          deriving (Show)

data Sep = SepLpar
         | SepRpar
         | SepLbrace
         | SepRbrace
         | SepLbracket
         | SepRbracket
         | SepSemi
         | SepComma
         deriving (Show)

data Op = OpAsn
        | OpOr
        | OpAnd
        | OpEq
        | OpNeq
        | OpLt
        | OpGt
        | OpLte
        | OpGte
        | OpPlus
        | OpMinus
        | OpMul
        | OpDiv
        | OpNot
        | OpInc
        | OpDec
        deriving (Show)

data TBool = Ttrue | Tfalse
           deriving (Show)

data Token = TokenKeyw Keyw
           | TokenBool TBool
           | TokenSep Sep
           | TokenOp Op
           | TokenId Text
           | TokenInt Text
           | TokenStr Text
           deriving (Show)
}
