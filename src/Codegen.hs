{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}

{-# HLINT ignore "Use camelCase" #-}

module Codegen (compileProgram) where

import Ast
import Control.Lens hiding (op)
import Control.Monad.State
import Data.List
import qualified Data.Map.Strict as M
import Data.Maybe
import Data.Text (Text)
import qualified Data.Text as T
import TypeChecker (TcAnn (TcAnn), typ)
import Utils

data Location = IlArg | IlVar
  deriving (Show, Read, Eq)

data CilVal = CilVal Location Type Int
  deriving (Show, Read, Eq)

data Instr
  = Label Integer
  | IassemblyExtern Text
  | Iassembly Text
  | Imaxstack Integer
  | Ientrypoint
  | Ilocals [(Text, Type)]
  | -- | int32
    Ipop
  | Ildc_i4 Int
  | -- | int32 0, used for boolean false
    Ildci_4_0
  | -- | int32 1, used for boolean true
    Ildci_4_1
  | IldStr Text
  | -- | assembly, return, method, params
    Icall Text Type Text [Type]
  | Idup
  | Iret
  | Iadd
  | Isub
  | Imul
  | Idiv
  | Iceq
  | Ineg
  | Iclt
  | Icgt
  | Ibr_s Integer
  | Ibrfalse_s Integer
  | Ibrtrue_s Integer
  | Ildloc_s Text
  | Istloc_s Text
  | Ildarg_s Text
  | Istarg_s Text
  | Inewarr PrimitiveType
  | Ildelem_i4
  | Istelem_i4
  deriving (Show, Read, Eq)

data CilMethodHeader = CilMethodHeader Int Type [(Type, Text)]
  deriving (Show, Read, Eq)

data CodegenState = CodegenState
  { _topLevel :: [Instr],
    _methodHeaders :: M.Map Text CilMethodHeader,
    _methodDefs :: M.Map Text [Instr],
    -- | Temporary buffer for adding instructions.
    _instrBuffer :: [Instr],
    _nextLabel :: Integer,
    _bindingLocs :: M.Map Text Location
  }
  deriving (Show, Read, Eq)

makeLenses ''CodegenState

initState :: CodegenState
initState =
  CodegenState
    { _topLevel = mempty,
      _methodHeaders = mempty,
      _methodDefs = mempty,
      _instrBuffer = mempty,
      _nextLabel = 0,
      _bindingLocs = mempty
    }

assemblyName :: Text
assemblyName = "Program"

newtype Codegen a = Codegen {runCodegen :: State CodegenState a}
  deriving (Functor, Applicative, Monad, MonadState CodegenState)

emit :: Instr -> Codegen ()
emit instr = instrBuffer %= (instr :)

newLabelId :: Codegen Integer
newLabelId = do
  x <- use nextLabel
  nextLabel += 1
  pure x

rename :: Text -> Text
rename = ("_" <>)

emitExpr :: Expr TcAnn -> Codegen ()
emitExpr = \case
  IntExpr _ x -> emit $ Ildc_i4 x
  BoolExpr _ x -> emit $ if x then Ildci_4_1 else Ildci_4_0
  StringExpr _ s -> emit $ IldStr s
  Bop _ e1 Add e2 -> emitExpr e1 >> emitExpr e2 >> emit Iadd
  Bop _ e1 Sub e2 -> emitExpr e1 >> emitExpr e2 >> emit Isub
  Bop _ e1 Mul e2 -> emitExpr e1 >> emitExpr e2 >> emit Imul
  Bop _ e1 Div e2 -> emitExpr e1 >> emitExpr e2 >> emit Idiv
  Bop _ e1 Eq e2 -> emitExpr e1 >> emitExpr e2 >> emit Iceq
  Bop _ e1 Neq e2 -> do
    emitExpr e1
    emitExpr e2
    emit Iceq
    emit Ildci_4_0
    emit Iceq
  Bop _ e1 Lt e2 -> emitExpr e1 >> emitExpr e2 >> emit Iclt
  Bop _ e1 Gt e2 -> emitExpr e1 >> emitExpr e2 >> emit Icgt
  Bop _ e1 Lte e2 -> do
    emitExpr e1
    emitExpr e2
    emit Icgt
    emit Ildci_4_0
    emit Iceq
  Bop _ e1 Gte e2 -> do
    emitExpr e1
    emitExpr e2
    emit Iclt
    emit Ildci_4_0
    emit Iceq
  Bop _ e1 And e2 -> do
    shortCircuit <- newLabelId
    end <- newLabelId
    emitExpr e1
    emit $ Ibrfalse_s shortCircuit
    emitExpr e2
    emit $ Ibr_s end
    emit $ Label shortCircuit
    emit Ildci_4_0
    emit $ Label end
  Bop _ e1 Or e2 -> do
    right <- newLabelId
    end <- newLabelId
    emitExpr e1
    emit $ Ibrfalse_s right
    emit Ildci_4_1
    emit $ Ibr_s end
    emit $ Label right
    emitExpr e2
    emit $ Label end
  Uop _ Not e -> emitExpr e >> emit Ildci_4_0 >> emit Iceq
  Uop _ Negation e -> emitExpr e >> emit Ineg
  Identifier _ name -> do
    loc <- fromJust . M.lookup name <$> use bindingLocs
    emit $ case loc of
      IlArg -> Ildarg_s name
      IlVar -> Ildloc_s name
  Assignment _ (VarName _ name) e -> do
    loc <- fromJust . M.lookup name <$> use bindingLocs
    emitExpr e
    emit Idup
    case loc of
      IlArg -> emit (Istarg_s name)
      IlVar -> emit (Istloc_s name)
  Assignment _ (IndexDest _ name idx) e -> do
    loc <- fromJust . M.lookup name <$> use bindingLocs
    emitExpr e -- Return value
    case loc of
      IlArg -> emit (Ildarg_s name)
      IlVar -> emit (Ildloc_s name)
    emitExpr idx
    emitExpr e
    emit Istelem_i4
  Funcall _ "print" [e] -> do
    emitExpr e
    case typ (exprAnn e) of
      t@(Prim _) ->
        emit $ Icall "System.Console" (Prim Void) "WriteLine" [t]
      typ' -> error $ "print was not typechecked correctly, got " <> show typ'
  Funcall (TcAnn {typ}) name exprs -> do
    mapM_ emitExpr exprs
    headers <- use methodHeaders
    let CilMethodHeader _ _ params =
          case M.lookup name headers of
            Just h -> h
            Nothing -> error $ "not found: " <> T.unpack name
    emit $ Icall assemblyName typ name $ map fst params
  Inc _ name -> do
    emit $ Ildloc_s name
    emit Ildci_4_1
    emit Iadd
    emit Idup
    emit $ Istloc_s name
  Dec _ name -> do
    emit $ Ildloc_s name
    emit Ildci_4_1
    emit Isub
    emit Idup
    emit $ Istloc_s name
  New _ t size -> do
    emitExpr size
    emit $ Inewarr t
  Index _ e idx -> do
    emitExpr e
    emitExpr idx
    emit Ildelem_i4

emitBlock :: [BlockStmt TcAnn] -> Codegen ()
emitBlock = mapM_ emitBlockStmt

emitBlockStmt :: BlockStmt TcAnn -> Codegen ()
emitBlockStmt = \case
  NormalStmt _ s -> emitStmt s
  VarStmt _ (VarDecl _ name _) expr -> do
    emitExpr expr
    emit $ Istloc_s name

emitStmt :: Stmt TcAnn -> Codegen ()
emitStmt = \case
  Block _ stmts -> emitBlock stmts
  (If _ expr stmt) -> do
    end <- newLabelId
    emitExpr expr
    emit $ Ibrfalse_s end
    emitStmt stmt
    emit $ Label end
  (IfElse _ expr s1 s2) -> do
    elseBranch <- newLabelId
    end <- newLabelId
    emitExpr expr
    emit $ Ibrfalse_s elseBranch
    emitStmt s1
    emit $ Ibr_s end
    emit $ Label elseBranch
    emitStmt s2
    emit $ Label end
  (While _ expr stmt) -> do
    start <- newLabelId
    end <- newLabelId
    emit $ Label start
    emitExpr expr
    emit $ Ibrfalse_s end
    emitStmt stmt
    emit $ Ibr_s start
    emit $ Label end
  (For _ before cond after stmt) -> do
    start <- newLabelId
    end <- newLabelId
    mapM_ emitExpr before
    emit Ipop
    emit $ Label start
    mapM_ emitExpr cond
    emit $ Ibrfalse_s end
    emitStmt stmt
    mapM_ emitExpr after
    emit Ipop
    emit $ Ibr_s start
    emit $ Label end
  (Return _ (Just e)) -> emitExpr e >> emit Iret
  (Return _ Nothing) -> emit Iret
  (ExprStmt _ e) -> emitExpr e >> unless (exprType == Prim Void) (emit Ipop)
    where
      exprType = typ $ exprAnn e

moveInstrBufferToMethod :: Text -> Codegen ()
moveInstrBufferToMethod name = do
  buf <- reverse <$> use instrBuffer
  methodDefs %= M.insert name buf
  instrBuffer .= mempty
  nextLabel .= 0

getVariables :: [BlockStmt a] -> [(Text, Type)]
getVariables = concatMap getBlockVariables
  where
    getBlockVariables :: BlockStmt a -> [(Text, Type)]
    getBlockVariables = \case
      NormalStmt _ s -> getStmtVariables s
      VarStmt _ (VarDecl _ name typ) _ -> pure (name, typ)
    getStmtVariables = \case
      (Block _ s) -> concatMap getBlockVariables s
      (If _ _ s) -> getStmtVariables s
      (IfElse _ _ s1 s2) -> getStmtVariables s1 <> getStmtVariables s2
      (While _ _ s) -> getStmtVariables s
      (For _ _ _ _ s) -> getStmtVariables s
      (Return _ _) -> []
      (ExprStmt _ _) -> []

emitDecl :: Decl TcAnn -> Codegen ()
emitDecl (Decl _ _ name formals body) = do
  when (name == "main") $
    emit Ientrypoint
  let bindings = getVariables body
  unless (null bindings) $ emit $ Ilocals bindings
  bindingLocs
    .= M.fromList (map argLocs formals <> map (\(n, _) -> (n, IlVar)) bindings)
  mapM_ emitBlockStmt body
  ensureFinalRet
  moveInstrBufferToMethod name
  where
    argLocs (VarDecl _ n _) = (n, IlArg)
    ensureFinalRet =
      use instrBuffer >>= \case
        Iret : _ -> pure ()
        [] -> pure ()
        _ : _ -> emit Iret

moveInstrBufferToTopLevel :: Codegen ()
moveInstrBufferToTopLevel = do
  buf <- reverse <$> use instrBuffer
  topLevel .= buf
  instrBuffer .= mempty

emitProgram :: Program TypeChecked TcAnn -> Codegen ()
emitProgram prgm = do
  let decls = runProgram prgm
  methodHeaders .= M.fromList (zipWith methodHeader [0 ..] decls)
  emit $ IassemblyExtern "System.Runtime"
  emit $ IassemblyExtern "System.Console"
  emit $ Iassembly assemblyName
  moveInstrBufferToTopLevel
  mapM_ emitDecl decls
  where
    methodHeader x (Decl _ ret name params _) =
      ( name,
        CilMethodHeader x ret $ declPairs params
      )
    declPairs = map $ \(VarDecl _ n t) -> (t, n)

showCilPrimType :: PrimitiveType -> Text
showCilPrimType = \case
  Void -> "void"
  Int -> "int32"
  Bool -> "int32"
  Str -> "string"

showCilType :: Type -> Text
showCilType = \case
  Arr t -> showCilPrimType t <> "[]"
  Prim t -> showCilPrimType t

showCilPrimTypeForFuncall :: PrimitiveType -> Text
showCilPrimTypeForFuncall = \case
  Void -> "void"
  Int -> "int32"
  Bool -> "bool"
  Str -> "string"

showCilTypeForFuncall :: Type -> Text
showCilTypeForFuncall = \case
  Arr t -> showCilPrimTypeForFuncall t <> "[]"
  Prim t -> showCilPrimTypeForFuncall t

indentLength :: Int
indentLength = 4

indent :: Int -> Text -> Text
indent n = (T.pack (replicate (indentLength * n) ' ') <>)

labelPrefix :: Text
labelPrefix = "label"

printLabel :: Instr -> Text
printLabel = \case
  Label idx -> labelPrefix <> T.pack (show idx) <> ":"
  IassemblyExtern name -> ".assembly extern " <> name <> " {}"
  Iassembly name -> ".assembly " <> name <> " {}"
  Imaxstack x -> ".maxstack " <> T.pack (show x)
  Ientrypoint -> ".entrypoint"
  Ipop -> "pop"
  Ildc_i4 x -> "ldc.i4 " <> T.pack (show x)
  Ildci_4_0 -> "ldc.i4.0"
  Ildci_4_1 -> "ldc.i4.1"
  IldStr s -> "ldstr " <> s
  Icall assembly ret name params ->
    -- Handle calls to user-defined functions and .NET functions differently.
    let (name', showParam) =
          if assembly == assemblyName
            then (rename name, showCilType)
            else (name, showCilTypeForFuncall)
     in T.concat
          [ "call ",
            showCilType ret,
            " [",
            assembly,
            "]",
            " ",
            assembly,
            "::",
            name',
            "(",
            T.intercalate ", " (map showParam params),
            ")"
          ]
  Idup -> "dup"
  Iret -> "ret"
  Iadd -> "add"
  Isub -> "sub"
  Imul -> "mul"
  Idiv -> "div"
  Iceq -> "ceq"
  Ineg -> "neg"
  Iclt -> "clt"
  Icgt -> "cgt"
  Ibr_s x -> "br.s " <> labelPrefix <> showText x
  Ibrfalse_s x -> "brfalse.s " <> labelPrefix <> showText x
  Ibrtrue_s x -> "brtrue.s " <> labelPrefix <> showText x
  Ildloc_s x -> "ldloc.s " <> rename x
  Istloc_s x -> "stloc.s " <> rename x
  Ildarg_s x -> "ldarg.s " <> rename x
  Istarg_s x -> "starg.s " <> rename x
  Ilocals binds ->
    ".locals init (\n"
      <> T.intercalate ",\n" (map (indent 3 . showBind) binds)
      <> "\n"
      <> indent 2 ")"
    where
      showBind (name, typ) = showCilType typ <> " " <> rename name
  Inewarr t -> "newarr " <> showCilPrimType t
  Ildelem_i4 -> "ldelem.i4"
  Istelem_i4 -> "stelem.i4"

compileMethod :: Type -> Text -> [(Type, Text)] -> [Instr] -> Text
compileMethod ret name params body =
  let sig =
        T.concat
          [ ".method public static ",
            showCilType ret,
            " ",
            rename name,
            "(",
            T.intercalate
              ", "
              (map (\(typ, n) -> showCilType typ <> " " <> rename n) params),
            ")",
            " cil managed"
          ]
      body' = T.intercalate "\n" $ map (indent 2 . printLabel) body
   in indent 1 sig
        <> "\n"
        <> indent 1 "{"
        <> "\n"
        <> body'
        <> "\n"
        <> indent 1 "}"

compileProgram :: Program TypeChecked TcAnn -> Text
compileProgram prgm =
  let st = execState (runCodegen (emitProgram prgm)) initState
      top = st ^. topLevel
      headers = st ^. methodHeaders
      defs = st ^. methodDefs
      pairMethod (CilMethodHeader i ret params) body = (i, ret, params, body)
      printMethod (name', (_, ret, params, body)) =
        compileMethod ret name' params body
      printedTopLevel = map printLabel top
      printedMethods =
        map printMethod $
          sortOn (\(_, (i, _, _, _)) -> i) $
            M.toList $
              M.intersectionWith pairMethod headers defs
      wrapInClass strs = [".class public Program\n{"] <> strs <> ["}"]
   in T.intercalate "\n" $
        printedTopLevel <> wrapInClass printedMethods <> ["\n"]
