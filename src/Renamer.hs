{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Renamer (runRename) where

import Ast
import Control.Lens hiding (op)
import Control.Monad.State
import qualified Data.Map.Strict as M
import Data.Text (Text)
import Utils

-- | A map of the original variable name to the new one.
type NameMap = M.Map Text Text

-- | A stack with the topmost element representing the current block.
type Scope = [NameMap]

data RenameState = RenameState
  { _depth :: Int,
    _scope :: Scope
  }

makeLenses ''RenameState

initState :: RenameState
initState = RenameState 0 mempty

newtype Rename a = Rename {runR :: State RenameState a}
  deriving (Functor, Applicative, Monad, MonadState RenameState)

-- | Modify the current NameMap (top of scope)
modifyNameMap :: (NameMap -> NameMap) -> Rename ()
modifyNameMap f = scope . ix 0 %= f

renameVar :: Text -> Rename Text
renameVar s = do
  d <- use depth
  pure $ s <> "_" <> showText d

insertVar :: Text -> Text -> Rename ()
insertVar s1 s2 = modifyNameMap $ M.insert s1 s2

lookupVar :: Text -> Rename (Maybe Text)
lookupVar s = msum . map (M.lookup s) <$> use scope

newBlock :: Rename ()
newBlock = scope %= (mempty :) >> depth += 1

popBlock :: Rename ()
popBlock = scope %= tail >> depth -= 1

resetMap :: Rename ()
resetMap = scope .= mempty >> modifyNameMap (const mempty)

renameExpr :: Expr a -> Rename (Expr a)
renameExpr = \case
  e@(IntExpr _ _) -> pure e
  e@(BoolExpr _ _) -> pure e
  e@(StringExpr _ _) -> pure e
  orig@(Identifier ann s) ->
    lookupVar s >>= \case
      Just s' -> pure $ Identifier ann s'
      Nothing -> pure orig
  Bop ann e1 op e2 -> do
    e1' <- renameExpr e1
    e2' <- renameExpr e2
    pure $ Bop ann e1' op e2'
  Uop ann op e -> do
    e' <- renameExpr e
    pure $ Uop ann op e'
  Funcall ann s es -> do
    es' <- mapM renameExpr es
    pure $ Funcall ann s es'
  orig@(Inc ann s) ->
    lookupVar s >>= \case
      Just s' -> pure $ Inc ann s'
      Nothing -> pure orig
  orig@(Dec ann s) ->
    lookupVar s >>= \case
      Just s' -> pure $ Dec ann s'
      Nothing -> pure orig
  New ann t e -> New ann t <$> renameExpr e
  Index ann e1 e2 -> liftM2 (Index ann) (renameExpr e1) (renameExpr e2)
  Assignment ann (VarName ann' s) e -> do
    e' <- renameExpr e
    lookupVar s >>= \case
      Just s' -> pure $ Assignment ann (VarName ann' s') e'
      Nothing -> pure $ Assignment ann (VarName ann' s) e'
  Assignment ann (IndexDest ann' s idx) e -> do
    e' <- renameExpr e
    idx' <- renameExpr idx
    lookupVar s >>= \case
      Just s' -> pure $ Assignment ann (IndexDest ann' s' idx') e'
      Nothing -> pure $ Assignment ann (IndexDest ann' s idx') e'

renameVarDecl :: VarDecl a -> Rename (VarDecl a)
renameVarDecl (VarDecl ann name t) = do
  newName <- renameVar name
  insertVar name newName
  pure $ VarDecl ann newName t

renameBlock :: [BlockStmt a] -> Rename [BlockStmt a]
renameBlock b = do
  newBlock
  b' <- mapM renameBlockStmt b
  popBlock
  pure b'

renameBlockStmt :: BlockStmt a -> Rename (BlockStmt a)
renameBlockStmt = \case
  VarStmt ann s e -> liftM2 (VarStmt ann) (renameVarDecl s) (renameExpr e)
  NormalStmt ann s -> NormalStmt ann <$> renameStmt s

renameStmt :: Stmt a -> Rename (Stmt a)
renameStmt = \case
  Block ann b -> Block ann <$> renameBlock b
  If ann e s -> do
    e' <- renameExpr e
    s' <- renameStmt s
    pure $ If ann e' s'
  IfElse ann e s1 s2 -> do
    e' <- renameExpr e
    s1' <- renameStmt s1
    s2' <- renameStmt s2
    pure $ IfElse ann e' s1' s2'
  While ann e s -> do
    e' <- renameExpr e
    s' <- renameStmt s
    pure $ While ann e' s'
  For ann before cond after stmt -> do
    before' <- mapM renameExpr before
    cond' <- mapM renameExpr cond
    after' <- mapM renameExpr after
    stmt' <- renameStmt stmt
    pure $ For ann before' cond' after' stmt'
  Return ann e -> Return ann <$> mapM renameExpr e
  ExprStmt ann e -> ExprStmt ann <$> renameExpr e

renameDecl :: Decl a -> Rename (Decl a)
renameDecl (Decl ann t name formals body) = do
  newBlock
  formals' <- mapM renameVarDecl formals
  body' <- renameBlock body
  popBlock
  pure $ Decl ann t name formals' body'

renameProgram :: Program Unmodified a -> Rename (Program Renamed a)
renameProgram prog =
  Program
    <$> mapM
      (\d -> do d' <- renameDecl d; resetMap; pure d')
      (runProgram prog)

runRename :: Program Unmodified a -> Program Renamed a
runRename p = evalState (runR (renameProgram p)) initState
