{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module TypeChecker (runTypechecker, TcAnn (TcAnn), position, typ) where

import Ast
import Control.Lens hiding (op)
import Control.Monad.Except
import Control.Monad.State
import Data.List (findIndex)
import qualified Data.Map.Strict as M
import Data.Text (Text)
import Parser
import Utils

type TypeEnv = M.Map Text Type

data TcAnn = TcAnn {position :: Pos, typ :: Type}
  deriving (Read, Show, Eq)

data TcState = TcState
  { _funEnv :: FunEnv,
    -- | No stack needed since return types are simply checked against the
    -- function return type.
    _typeEnv :: TypeEnv,
    -- | Keeps track of name and return type of the current function.
    _currentFunInfo :: (Text, Type)
  }

makeLenses ''TcState

initialState :: TcState
initialState =
  TcState
    { _funEnv = initialFunEnv,
      _typeEnv = mempty,
      _currentFunInfo = undefined
    }

newtype TC a = TC {runTC :: ExceptT SrcError (State TcState) a}
  deriving
    ( Functor,
      Applicative,
      Monad,
      MonadError SrcError,
      MonadState TcState
    )

-- | Used to find out if control reaches end of non-void function, returning the
-- position of the if statement that causes it. Done before the rest of the type
-- checking.
data EndCheck = HasRet | NoRet | SingleRet Pos

-- | If an if statement guarantees return, then we
-- require that either the else branch or the rest of the function has
-- a return statement that is guaranteed.
checkEndSingle :: BlockStmt Pos -> EndCheck
checkEndSingle = \case
  NormalStmt _ s -> checkEndNormalStmt s
  VarStmt {} -> NoRet

checkEndNormalStmt :: Stmt Pos -> EndCheck
checkEndNormalStmt = \case
  Block _ stmts -> checkEndBlock stmts
  IfElse pos _ s1 s2 -> case (checkEndNormalStmt s1, checkEndNormalStmt s2) of
    (r@(SingleRet _), _) -> r
    (_, r@(SingleRet _)) -> r -- Propagate error upwards
    (HasRet, NoRet) -> SingleRet pos
    (NoRet, HasRet) -> SingleRet pos
    (NoRet, NoRet) -> NoRet
    (HasRet, HasRet) -> HasRet
  If pos _ s' -> case checkEndNormalStmt s' of
    HasRet -> SingleRet pos
    NoRet -> NoRet
    r@(SingleRet _) -> r
  While _ _ stmt -> checkEndNormalStmt stmt
  For _ _ _ _ stmt -> checkEndNormalStmt stmt
  Return {} -> HasRet
  ExprStmt _ _ -> NoRet

checkEndBlock :: [BlockStmt Pos] -> EndCheck
checkEndBlock = \case
  [] -> NoRet
  [s] -> checkEndSingle s
  (NormalStmt _ s@If {}) : rest ->
    -- Immediate child - error salvageable if xs has ret
    case checkEndNormalStmt s of
      r@(SingleRet _) -> case checkEndBlock rest of
        HasRet -> HasRet
        _ -> r
      _ -> checkEndBlock rest
  NormalStmt _ s@IfElse {} : rest ->
    case checkEndNormalStmt s of
      r@(SingleRet _) -> case checkEndBlock rest of
        HasRet -> HasRet
        _ -> r
      _ -> checkEndBlock rest
  s : rest ->
    case checkEndSingle s of
      r@(SingleRet _) -> r
      _ -> checkEndBlock rest

checkEndGuard :: Text -> Function -> TC ()
checkEndGuard name = \case
  (Def (Prim Void) _ _ _) -> pure ()
  Print -> pure ()
  (Def t _ body pos) ->
    case checkEndBlock body of
      HasRet -> pure ()
      NoRet -> throwError $ NoReturnStatement name t pos
      SingleRet pos' -> throwError $ PotentialEndWithoutReturn name t pos'

lookupVar :: Text -> TC (Maybe Type)
lookupVar s = M.lookup s <$> use typeEnv

lookupFun :: Text -> TC (Maybe Function)
lookupFun s = M.lookup s <$> use funEnv

varIsDeclared :: Text -> TC Bool
varIsDeclared s = M.member s <$> use typeEnv

funIsDeclared :: Text -> TC Bool
funIsDeclared name = M.member name <$> use funEnv

-- | Ensure expr is of type t
(-->) :: Expr Pos -> Type -> TC (Expr TcAnn)
e --> t = do
  checkedExpr <- tcExpr e
  let t' = typ $ exprAnn checkedExpr
  ensure (t == t') $ ExpectedType t t' (exprAnn e)
  pure checkedExpr

-- | Ensure expr is NOT of type t
(-/->) :: Expr Pos -> Type -> TC (Expr TcAnn)
e -/-> t = do
  checkedExpr <- tcExpr e
  let t' = typ $ exprAnn checkedExpr
  ensure (t /= t') $ UnexpectedType t' (exprAnn e)
  pure checkedExpr

tcArithOp :: Expr Pos -> Bop -> Expr Pos -> Pos -> TC (Expr TcAnn)
tcArithOp e1 op e2 pos = do
  e1' <- e1 --> Prim Int
  e2' <- e2 --> Prim Int
  pure $ Bop TcAnn {position = pos, typ = Prim Int} e1' op e2'

tcCmpOp :: Expr Pos -> Bop -> Expr Pos -> Pos -> TC (Expr TcAnn)
tcCmpOp e1 op e2 pos = do
  e1' <- e1 --> Prim Int
  e2' <- e2 --> Prim Int
  pure $ Bop TcAnn {position = pos, typ = Prim Bool} e1' op e2'

tcBoolOp :: Expr Pos -> Bop -> Expr Pos -> Pos -> TC (Expr TcAnn)
tcBoolOp e1 op e2 pos = do
  e1' <- e1 --> Prim Bool
  e2' <- e2 --> Prim Bool
  pure $ Bop TcAnn {position = pos, typ = Prim Bool} e1' op e2'

tcUop :: Uop -> Expr Pos -> Pos -> TC (Expr TcAnn)
tcUop Not e pos = do
  e' <- e --> Prim Bool
  pure $ Uop TcAnn {position = pos, typ = Prim Bool} Not e'
tcUop Negation e pos = do
  e' <- e --> Prim Int
  pure $ Uop TcAnn {position = pos, typ = Prim Int} Negation e'

tcIdentifier :: Text -> Pos -> TC Type
tcIdentifier name pos =
  lookupVar name >>= \case
    Just t' -> return t'
    Nothing -> throwError $ UndeclaredVariableUsage name pos

tcAsn :: Pos -> Text -> Expr Pos -> TC (Expr TcAnn)
tcAsn pos name expr = do
  checkedExpr <- expr -/-> Prim Void
  varType <- lookupVar name
  let exprType = typ $ exprAnn checkedExpr
  case varType of
    Just varType' -> do
      ensure (varType' == exprType) $ ExpectedType varType' exprType pos
      pure $
        let ann = TcAnn {position = pos, typ = exprType}
         in Assignment ann (VarName ann name) checkedExpr
    Nothing -> throwError $ UndeclaredVariableAssignment name pos

tcIndexAsn :: Pos -> Text -> Expr Pos -> Expr Pos -> TC (Expr TcAnn)
tcIndexAsn pos name idx expr = do
  checkedIdx <- idx --> Prim Int
  checkedExpr <- expr -/-> Prim Void
  varType <- lookupVar name
  let exprType = typ $ exprAnn checkedExpr
  case varType of
    Just (Arr elemType) -> do
      ensure (Prim elemType == exprType) $ ExpectedType (Prim elemType) exprType pos
      let ann = TcAnn {position = pos, typ = exprType}
      pure $ Assignment ann (IndexDest ann name checkedIdx) checkedExpr
    Just t -> throwError $ IndexNonArray t pos
    Nothing -> throwError $ UndeclaredVariableAssignment name pos

firstVoidIdx :: [Expr Pos] -> TC (Either Int [Expr TcAnn])
firstVoidIdx exprs = do
  checkedExprs <- mapM tcExpr exprs
  let idx = (+ 1) <$> findIndex (\e -> typ (exprAnn e) == Prim Void) checkedExprs
  pure $ case idx of
    Just idx' -> Left idx'
    Nothing -> Right checkedExprs

checkArg :: VarDecl Pos -> Expr Pos -> TC (Expr TcAnn)
checkArg (VarDecl _ _ t) expr = do
  checkedExpr <- tcExpr expr
  let t' = typ $ exprAnn checkedExpr
  ensure (t' == t) $ ExpectedType t t' (exprAnn expr)
  return checkedExpr

checkArgs :: Text -> Pos -> [VarDecl Pos] -> [Expr Pos] -> TC [Expr TcAnn]
checkArgs name pos formals exprs = do
  let formalLen = length formals
  let exprsLen = length exprs
  ensure (formalLen == exprsLen) $
    CalledWithWrongArgCount name formalLen exprsLen pos
  zipWithM checkArg formals exprs

tcFuncall :: Text -> Pos -> [Expr Pos] -> TC (Expr TcAnn)
tcFuncall name pos exprs = do
  f <- lookupFun name
  case f of
    Just (Def t formals _ _) -> do
      exprs' <- checkArgs name pos formals exprs
      let ann = TcAnn {typ = t, position = pos}
      pure $ Funcall ann name exprs'
    Just Print -> do
      exprs' <-
        firstVoidIdx exprs >>= \case
          Right [e] -> pure [e]
          Right _ -> throwError $ CalledWithWrongArgCount "print" 1 (length exprs) pos
          Left _ -> throwError $ UnexpectedType (Prim Void) pos
      pure $ Funcall TcAnn {position = pos, typ = Prim Void} name exprs'
    Nothing -> throwError $ UndefinedFunctionUsage name pos

tcEqExpr :: Bool -> Expr Pos -> Expr Pos -> Pos -> TC (Expr TcAnn)
tcEqExpr doNegate e1 e2 pos = do
  e1' <- tcExpr e1
  e2' <- tcExpr e2
  let (t1, t2) = (typ (exprAnn e1'), typ (exprAnn e2'))
  ensure (t1 /= Prim Void) $ UnexpectedType (Prim Void) (exprAnn e1)
  ensure (t2 /= Prim Void) $ UnexpectedType (Prim Void) (exprAnn e2)
  ensure (t2 == t1) $ ExpectedType t1 t2 (position (exprAnn e2'))
  pure $ Bop (TcAnn pos (Prim Bool)) e1' (if doNegate then Neq else Eq) e2'

tcExpr :: Expr Pos -> TC (Expr TcAnn)
tcExpr = \case
  e@(IntExpr _ _) -> e `decoratedWith` Prim Int
  e@(BoolExpr _ _) -> e `decoratedWith` Prim Bool
  e@(StringExpr _ _) -> e `decoratedWith` Prim Str
  e@(Identifier pos name) -> do
    t <- tcIdentifier name pos
    e `decoratedWith` t
  Bop pos e1 Eq e2 -> tcEqExpr False e1 e2 pos
  Bop pos e1 Neq e2 -> tcEqExpr True e1 e2 pos
  Bop pos e1 Add e2 -> tcArithOp e1 Add e2 pos
  Bop pos e1 Sub e2 -> tcArithOp e1 Sub e2 pos
  Bop pos e1 Mul e2 -> tcArithOp e1 Mul e2 pos
  Bop pos e1 Div e2 -> tcArithOp e1 Div e2 pos
  Bop pos e1 Lt e2 -> tcCmpOp e1 Lt e2 pos
  Bop pos e1 Gt e2 -> tcCmpOp e1 Gt e2 pos
  Bop pos e1 Lte e2 -> tcCmpOp e1 Lte e2 pos
  Bop pos e1 Gte e2 -> tcCmpOp e1 Gte e2 pos
  Bop pos e1 And e2 -> tcBoolOp e1 And e2 pos
  Bop pos e1 Or e2 -> tcBoolOp e1 Or e2 pos
  Uop pos op e -> tcUop op e pos
  Funcall pos name exprs -> tcFuncall name pos exprs
  Assignment pos (VarName _ n) expr -> tcAsn pos n expr
  Assignment pos (IndexDest _ n idx) expr -> tcIndexAsn pos n idx expr
  e@(Inc pos name) -> tcIntIdentifier name pos e
  e@(Dec pos name) -> tcIntIdentifier name pos e
  New pos primType idx -> do
    idx' <- idx --> Prim Int
    pure $ New (TcAnn pos (Arr primType)) Int idx'
  Index pos e idx -> do
    e' <- tcExpr e
    idx' <- idx --> Prim Int
    case typ $ exprAnn e' of
      Arr t -> pure $ Index (TcAnn pos (Prim t)) e' idx'
      t -> throwError $ IndexNonArray t pos
  where
    e `decoratedWith` t = pure $ fmap (`TcAnn` t) e
    tcIntIdentifier name pos origExpr = do
      t <- tcIdentifier name pos
      ensure (t == Prim Int) $ ExpectedType t (Prim Int) pos
      origExpr `decoratedWith` t

tcBlockStmt :: BlockStmt Pos -> TC (BlockStmt TcAnn)
tcBlockStmt = \case
  NormalStmt pos s -> NormalStmt (TcAnn pos (Prim Void)) <$> tcStmt s
  VarStmt _ (VarDecl pos name t) e -> do
    e' <- e --> t
    ensureM (not <$> varIsDeclared name) $
      VariableAlreadyDeclared
        (displayVar name)
        pos
    typeEnv %= M.insert name t
    let ann = TcAnn pos (Prim Void)
    pure $ VarStmt ann (VarDecl ann name t) e'

tcStmt :: Stmt Pos -> TC (Stmt TcAnn)
tcStmt = \case
  Block pos stmts -> Block (TcAnn pos (Prim Void)) <$> mapM tcBlockStmt stmts
  If pos expr stmt -> do
    expr' <- expr --> Prim Bool
    stmt' <- tcStmt stmt
    pure $ If (TcAnn pos (Prim Void)) expr' stmt'
  IfElse pos expr s1 s2 -> do
    expr' <- expr --> Prim Bool
    s1' <- tcStmt s1
    s2' <- tcStmt s2
    pure $ IfElse (TcAnn pos (Prim Void)) expr' s1' s2'
  While pos expr s -> do
    expr' <- expr --> Prim Bool
    s' <- tcStmt s
    pure $ While (TcAnn pos (Prim Void)) expr' s'
  For pos before cond after s -> do
    cond' <- mapM (--> Prim Bool) cond
    before' <- mapM tcExpr before
    after' <- mapM tcExpr after
    s' <- tcStmt s
    pure $ For (TcAnn pos (Prim Void)) before' cond' after' s'
  Return pos (Just expr) -> do
    (_, funRet) <- use currentFunInfo
    checkedExpr <- tcExpr expr
    let exprType = typ $ exprAnn checkedExpr
    ensure (exprType == funRet) $ ReturnTypeNotMatching funRet exprType pos
    pure $ Return (TcAnn pos (Prim Void)) (Just checkedExpr)
  s@(Return pos Nothing) -> do
    (_, ret) <- use currentFunInfo
    ensure (ret == Prim Void) $ ExpectedType (Prim Void) ret pos
    pure $ fmap (`TcAnn` Prim Void) s
  ExprStmt pos e -> do
    checkedExpr <- tcExpr e
    pure $ ExprStmt (TcAnn pos (Prim Void)) checkedExpr

tcDecl :: Decl Pos -> TC (Decl TcAnn)
tcDecl (Decl pos t funName formals body) = do
  mapM_ checkParam formals
  typeEnv .= M.fromList (fmap (\(VarDecl _ name t') -> (name, t')) formals)
  currentFunInfo .= (funName, t)
  checkedBlock <- mapM tcBlockStmt body
  let annFormals = fmap markAsVoid formals
  pure $ Decl TcAnn {position = pos, typ = t} t funName annFormals checkedBlock
  where
    checkParam (VarDecl pos' _ t') =
      ensure (t' /= Prim Void) $ UnexpectedType (Prim Void) pos'
    markAsVoid = fmap (`TcAnn` Prim Void)

addFunction :: Decl Pos -> TC ()
addFunction f@(Decl pos _ name formals _) = do
  guardDuplicateParams name formals
  ensureM (not <$> funIsDeclared name) $ FunctionAlreadyDefined name pos
  funEnv %= M.insert name (declToFun f)

tcProgram :: Program Renamed Pos -> TC (Program TypeChecked TcAnn)
tcProgram prgm = do
  mapM_ addFunction $ runProgram prgm
  mapM_ (uncurry checkEndGuard) . M.toList =<< use funEnv
  ensureM (M.member "main" <$> use funEnv) MainNotDefined
  Program <$> mapM tcDecl (runProgram prgm)

runTypechecker ::
  Program Renamed Pos ->
  Either SrcError (Program TypeChecked TcAnn)
runTypechecker prgm =
  evalState
    (runExceptT (runTC (tcProgram prgm)))
    initialState
