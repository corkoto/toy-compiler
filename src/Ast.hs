{-# LANGUAGE DeriveFunctor #-}

module Ast where

import Data.Text (Text)
import Lexer

class HasPosition a where
  getPos :: a -> Pos

data Unmodified

data Renamed

data TypeChecked

newtype Program s a = Program {runProgram :: [Decl a]}
  deriving (Show, Read, Eq, Functor)

data Decl a = Decl a Type Text [VarDecl a] [BlockStmt a]
  deriving (Show, Read, Eq, Functor)

data VarDecl a = VarDecl a Text Type
  deriving (Show, Read, Eq, Functor)

data Type = Prim PrimitiveType | Arr PrimitiveType
  deriving (Show, Read, Eq)

data PrimitiveType = Void | Int | Bool | Str
  deriving (Show, Read, Eq)

-- Variable declarations should only appear in blocks and are therefore treated
-- specially.
data BlockStmt a = NormalStmt a (Stmt a) | VarStmt a (VarDecl a) (Expr a)
  deriving (Show, Read, Eq, Functor)

data Stmt a
  = Block a [BlockStmt a]
  | If a (Expr a) (Stmt a)
  | IfElse a (Expr a) (Stmt a) (Stmt a)
  | While a (Expr a) (Stmt a)
  | For a (Maybe (Expr a)) (Maybe (Expr a)) (Maybe (Expr a)) (Stmt a)
  | Return a (Maybe (Expr a))
  | ExprStmt a (Expr a)
  deriving (Show, Read, Eq, Functor)

data Bop
  = Add
  | Sub
  | Mul
  | Div
  | And
  | Or
  | Eq
  | Neq
  | Lt
  | Gt
  | Lte
  | Gte
  deriving (Show, Read, Eq)

data Uop
  = Not
  | Negation
  deriving (Show, Read, Eq)

data Expr a
  = IntExpr a Int
  | BoolExpr a Bool
  | StringExpr a Text
  | Bop a (Expr a) Bop (Expr a)
  | Identifier a Text
  | Uop a Uop (Expr a)
  | Funcall a Text [Expr a]
  | Assignment a (Lval a) (Expr a)
  | Inc a Text
  | Dec a Text
  | New a PrimitiveType (Expr a)
  | Index a (Expr a) (Expr a)
  deriving (Show, Read, Eq, Functor)

data Lval a = VarName a Text | IndexDest a Text (Expr a)
  deriving (Show, Read, Eq, Functor)

declAnn :: Decl a -> a
declAnn (Decl ann _ _ _ _) = ann

varAnn :: VarDecl a -> a
varAnn (VarDecl ann _ _) = ann

blockStmtAnn :: BlockStmt a -> a
blockStmtAnn (NormalStmt ann _) = ann
blockStmtAnn (VarStmt ann _ _) = ann

stmtAnn :: Stmt a -> a
stmtAnn (Block ann _) = ann
stmtAnn (If ann _ _) = ann
stmtAnn (IfElse ann _ _ _) = ann
stmtAnn (While ann _ _) = ann
stmtAnn (For ann _ _ _ _) = ann
stmtAnn (Return ann _) = ann
stmtAnn (ExprStmt ann _) = ann

exprAnn :: Expr a -> a
exprAnn (IntExpr ann _) = ann
exprAnn (BoolExpr ann _) = ann
exprAnn (StringExpr ann _) = ann
exprAnn (Bop ann _ _ _) = ann
exprAnn (Identifier ann _) = ann
exprAnn (Uop ann _ _) = ann
exprAnn (Funcall ann _ _) = ann
exprAnn (Inc ann _) = ann
exprAnn (Dec ann _) = ann
exprAnn (New ann _ _) = ann
exprAnn (Index ann _ _) = ann
exprAnn (Assignment ann _ _) = ann

lvalAnn :: Lval a -> a
lvalAnn (VarName ann _) = ann
lvalAnn (IndexDest ann _ _) = ann
