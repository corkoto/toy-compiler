{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE TemplateHaskell #-}

module PrettyPrinting (runPP) where

import Ast
import Control.Lens hiding (op)
import Control.Monad.Reader
import Data.Text (Text)
import Prettyprinter
import Prettyprinter.Render.Text (renderStrict)

class Pr a where
  pp :: a -> Text

instance Pr Type where
  pp (Prim t) = pp t
  pp (Arr t) = pp t <> "[]"

instance Pr PrimitiveType where
  pp Void = "void"
  pp Int = "int"
  pp Bool = "bool"
  pp Str = "string"

instance Pr Uop where
  pp Not = "!"
  pp Negation = "-"

instance Pr Bop where
  pp Add = "+"
  pp Sub = "-"
  pp Mul = "*"
  pp Div = "/"
  pp And = "&&"
  pp Or = "||"
  pp Eq = "=="
  pp Neq = "!="
  pp Lt = "<"
  pp Gt = ">"
  pp Lte = "<="
  pp Gte = ">="

-- | Typeclass describing operator precedence and associativity, used to figure
-- out when to apply parentheses.
class Op a where
  precOf :: a -> Int
  assocOf :: a -> Assoc

instance Op Bop where
  precOf Or = 2
  precOf And = 3
  precOf Eq = 4
  precOf Neq = 4
  precOf Lt = 5
  precOf Gt = 5
  precOf Lte = 5
  precOf Gte = 5
  precOf Add = 6
  precOf Sub = 6
  precOf Mul = 7
  precOf Div = 7
  assocOf Or = L
  assocOf And = L
  assocOf Eq = L
  assocOf Neq = L
  assocOf Lt = L
  assocOf Gt = L
  assocOf Lte = L
  assocOf Gte = L
  assocOf Add = L
  assocOf Sub = L
  assocOf Mul = L
  assocOf Div = L

instance Op Uop where
  precOf _ = 8
  assocOf _ = NA

text :: Text -> Doc ann
text s = pretty (s :: Text)

indentLen :: Int
indentLen = 4

indent' :: Doc ann -> Doc ann
indent' = indent indentLen

data Assoc = L | R | NA
  deriving (Show, Eq)

data PrinterState = PrinterState
  { _isOpposite :: Bool,
    _precedence :: Int
  }
  deriving (Show)

makeLenses ''PrinterState

initState :: PrinterState
initState = PrinterState {_isOpposite = False, _precedence = 0}

newtype Printer a = Printer {runPrinter :: Reader PrinterState a}
  deriving (Functor, Applicative, Monad, MonadReader PrinterState)

needParens :: Int -> Printer Bool
needParens prec = do
  outerPrec <- view precedence
  opposite <- view isOpposite
  pure $ prec < outerPrec || opposite && prec == outerPrec

exprAssoc :: Expr a -> Assoc
exprAssoc (Bop _ _ op _) = assocOf op
exprAssoc (Uop _ op _) = assocOf op
exprAssoc _ = NA

runPP :: Program s a -> Text
runPP p =
  renderStrict $
    layoutPretty defaultLayoutOptions $
      runReader (runPrinter $ prettyProg p) initState

prettyProg :: Program s a -> Printer (Doc ann)
prettyProg p = vsep <$> mapM prettyDecl (runProgram p)

prettyDecl :: Decl a -> Printer (Doc ann)
prettyDecl (Decl _ t name params stmts) = do
  params' <- prettyFormal params
  body <- prettyBlock stmts
  pure $
    text (pp t)
      <+> text name
        <> fillSep [params', line <> body]

prettyFormal :: [VarDecl a] -> Printer (Doc ann)
prettyFormal xs = tupled <$> mapM prettyTypeVar xs

prettyTypeVar :: VarDecl a -> Printer (Doc ann)
prettyTypeVar (VarDecl _ i t) = pure $ text (pp t) <+> text i

block :: Doc ann -> Doc ann
block x = text "{" <> fillSep [nest indentLen $ line <> x, line <> text "}"]

prettyBlock :: [BlockStmt a] -> Printer (Doc ann)
prettyBlock b = do
  b' <- vsep <$> mapM prettyBlockStmt b
  pure $ block b'

blockSep :: Stmt a -> Doc ann
blockSep (Block _ _) = text " "
blockSep _ = indent' line

prettyBlockStmt :: BlockStmt a -> Printer (Doc ann)
prettyBlockStmt = \case
  NormalStmt _ stmt -> prettyStmt stmt
  VarStmt _ varDecl expr -> do
    v <- prettyTypeVar varDecl
    e <- prettyExpr expr
    pure $ v <+> text "=" <+> e <> semi

prettyStmt :: Stmt a -> Printer (Doc ann)
prettyStmt = \case
  Block _ stmts -> prettyBlock stmts
  If _ expr stmt -> do
    expr' <- prettyExpr expr
    stmt' <- prettyStmt stmt
    pure $
      text "if"
        <+> parens expr'
          <> blockSep stmt
          <> stmt'
  IfElse _ expr stmt1 stmt2 -> do
    expr' <- prettyExpr expr
    stmt1' <- prettyStmt stmt1
    stmt2' <- prettyStmt stmt2
    -- Continue else on same line if the "true" branch is a block statement
    -- followed by another if statement or another block
    let elseSep = case (stmt1, stmt2) of
          (Block _ _, If {}) -> text " "
          (Block _ _, IfElse {}) -> text " "
          (Block _ _, Block _ _) -> text " "
          _ -> line
    let (</+>) = case stmt2 of
          Block _ _ -> (<+>)
          If {} -> (<+>)
          IfElse {} -> (<+>)
          _ -> (\x y -> indent' $ fillSep [x, y])
    pure $
      (text "if" <+> parens expr' <> indent' (blockSep stmt1) <> stmt1')
        <> elseSep
        <> text "else"
        </+> stmt2'
  While _ expr stmt -> do
    expr' <- prettyExpr expr
    stmt' <- prettyStmt stmt
    pure $ text "while" <+> parens expr' <> blockSep stmt <> stmt'
  For _ before cond after stmt -> do
    before' <- maybe (pure mempty) prettyExpr before
    cond' <- maybe (pure mempty) prettyExpr cond
    after' <- maybe (pure mempty) prettyExpr after
    stmt' <- prettyStmt stmt
    pure $
      text "for"
        <+> parens (before' <> semi <+> cond' <> semi <+> after')
          <> blockSep stmt
          <> stmt'
  Return _ Nothing -> pure $ text "pure" <> semi
  Return _ (Just expr) -> do
    expr' <- prettyExpr expr
    pure $ text "pure" <+> expr' <> semi
  ExprStmt _ expr -> (<> semi) <$> prettyExpr expr

localPrinter ::
  MonadReader PrinterState m =>
  (t -> m a) ->
  t ->
  Bool ->
  Int ->
  m a
localPrinter f x opp prec =
  local
    (const PrinterState {_precedence = prec, _isOpposite = opp})
    (f x)

prettyExpr :: Expr a -> Printer (Doc ann)
prettyExpr = \case
  IntExpr _ x -> pure $ pretty x
  BoolExpr _ n -> pure $ if n then text "true" else text "false"
  StringExpr _ s -> pure $ text s
  Bop _ expr1 op expr2 -> do
    expr1' <- localPrinter prettyExpr expr1 (exprAssoc expr1 == R) (precOf op)
    expr2' <- localPrinter prettyExpr expr2 (exprAssoc expr2 == L) (precOf op)
    let op' = text $ pp op
    let doc = mconcat [expr1', softline, op', softline, expr2']
    useParens <- needParens $ precOf op
    pure $ if useParens then parens doc else doc
  Identifier _ s -> pure $ text s
  Uop _ op expr -> do
    expr' <- localPrinter prettyExpr expr False (precOf op)
    let op' = text $ pp op
    let doc = op' <> expr'
    useParens <- needParens $ precOf op
    pure $ if useParens then parens doc else doc
  Funcall _ s exprs -> do
    exprs' <- mapM (\x -> localPrinter prettyExpr x False 0) exprs
    pure $ text s <> tupled exprs'
  Inc _ s -> pure $ text s <> text "++"
  Dec _ s -> pure $ text s <> text "--"
  New _ t e -> do
    e' <- prettyExpr e
    pure $ text "new" <+> text (pp t) <> brackets e'
  Index _ e1 e2 -> liftM2 (<+>) (prettyExpr e1) (brackets <$> prettyExpr e2)
  Assignment _ (VarName _ s) expr -> do
    expr' <- localPrinter prettyExpr expr (exprAssoc expr == L) 1
    let doc = text s <+> text "=" <+> expr'
    useParens <- needParens 1
    pure $ if useParens then parens doc else doc
  Assignment _ (IndexDest _ s indexExpr) expr -> do
    indexExpr' <- prettyExpr indexExpr
    expr' <- localPrinter prettyExpr expr (exprAssoc expr == L) 1
    let doc = text s <> brackets indexExpr' <+> text "=" <+> expr'
    useParens <- needParens 1
    pure $ if useParens then parens doc else doc
