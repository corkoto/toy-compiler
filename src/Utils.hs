{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Utils where

import Ast
import Control.Monad.Except
import Data.List (find)
import qualified Data.Map.Strict as M
import Data.Maybe (isJust)
import Data.Text (Text)
import qualified Data.Text as T
import Parser

data Function
  = Def Type [VarDecl Pos] [BlockStmt Pos] Pos
  | Print
  deriving (Show)

data SrcError
  = MainNotDefined
  | -- | Function and variable name
    RepeatingParamName Text Text Pos
  | FunctionAlreadyDefined Text Pos
  | UndefinedFunctionUsage Text Pos
  | VariableAlreadyDeclared Text Pos
  | -- | Expected and actual type
    ReturnTypeNotMatching Type Type Pos
  | -- | Expected and actual type
    ExpectedType Type Type Pos
  | UnexpectedType Type Pos
  | UndeclaredVariableUsage Text Pos
  | UndeclaredVariableAssignment Text Pos
  | CalledWithWrongArgCount Text Int Int Pos -- Function name, expected, actual
  | NoReturnStatement Text Type Pos -- Function name and expected type
  | PotentialEndWithoutReturn Text Type Pos -- Function name, expected
  | IndexNonArray Type Pos
  deriving (Show, Eq)

showText :: Show a => a -> Text
showText = T.pack . show

-- | "Unrename" a renamed variable when displaying an error.
displayVar :: Text -> Text
displayVar = T.concat . init . T.splitOn "_"

showError :: SrcError -> Text
showError = \case
  MainNotDefined -> "No main function defined"
  RepeatingParamName f var pos ->
    "Repeating occurrence of parameter "
      <> var
      <> " in function "
      <> f
      <> at pos
  FunctionAlreadyDefined f pos -> "Redefinition of function " <> f <> at pos
  UndefinedFunctionUsage f pos -> "Use of undefined function " <> f <> at pos
  VariableAlreadyDeclared var pos ->
    "Redefinition of variable " <> var <> at pos
  ReturnTypeNotMatching expected actual pos ->
    "Returning value of type "
      <> showType actual
      <> " when "
      <> showType expected
      <> " was expected"
      <> at pos
  ExpectedType expected actual pos ->
    "Expected type "
      <> showType expected
      <> " but got value of type "
      <> showType actual
      <> at pos
  UnexpectedType typ pos -> "Unexpected value of type " <> showType typ <> at pos
  UndeclaredVariableUsage var pos ->
    "Use of undeclared variable "
      <> var
      <> at pos
  UndeclaredVariableAssignment var pos ->
    "Assignment to undeclared variable "
      <> var
      <> at pos
  CalledWithWrongArgCount f expected actual pos ->
    "Call to function "
      <> showText f
      <> " with "
      <> showText actual
      <> " arguments when "
      <> showText expected
      <> " was expected"
      <> at pos
  NoReturnStatement f expected pos ->
    "No return statement found in function "
      <> showText f
      <> ", but expected a return value of type "
      <> showType expected
      <> at pos
  PotentialEndWithoutReturn f expected pos ->
    "Potential end without return in function "
      <> showText f
      <> "; all branches should return a value of type "
      <> showType expected
      <> at pos
  IndexNonArray t pos ->
    "Attempted to index non-array "
      <> showType t
      <> at pos
  where
    showPrimType :: PrimitiveType -> Text
    showPrimType = \case
      Int -> "int"
      Bool -> "bool"
      Void -> "void"
      Str -> "string"
    showType :: Type -> Text
    showType = \case
      Prim t -> showPrimType t
      Arr t -> showPrimType t <> "[]"
    at (l, c) =
      T.concat
        [ "\nat line ",
          showText l,
          ", column ",
          showText c
        ]

declToFun :: Decl Pos -> Function
declToFun (Decl pos t _ formals body) = Def t formals body pos

type FunEnv = M.Map Text Function

-- | Initially defined function primitives.
initialFunEnv :: M.Map Text Function
initialFunEnv = M.fromList [("print", Print)]

-- | Throw an error if a monadic condition is not satisfied.
ensureM :: (MonadError SrcError m) => m Bool -> SrcError -> m ()
ensureM b e = do b' <- b; if b' then pure () else throwError e

-- | Throw an error if a condition is not satisfied.
ensure :: (MonadError SrcError m) => Bool -> SrcError -> m ()
ensure = ensureM . pure

-- | Find the first duplicate variable name in a list of declarations.
firstDupe :: [VarDecl Pos] -> Maybe (VarDecl Pos)
firstDupe [] = Nothing
firstDupe (x : xs) =
  if isJust $ find (nameEq x) xs
    then Just x
    else firstDupe xs
  where
    nameEq x' y = varName x' == varName y
    varName (VarDecl _ name _) = name

-- | Throw an error when parameter names are repeated.
guardDuplicateParams :: (MonadError SrcError m) => Text -> [VarDecl Pos] -> m ()
guardDuplicateParams funName formals =
  case firstDupe formals of
    Just (VarDecl pos name _) ->
      throwError $
        RepeatingParamName funName (displayVar name) pos
    Nothing -> return ()
