{
{-# LANGUAGE OverloadedStrings #-}
module Parser (module Parser, Pos) where

import Lexer
import Data.Text (Text)
import qualified Data.Text as T
import Ast
}

%name parse
%tokentype { (Token, Pos) }
%error { parseError }

%token
  if     { (TokenKeyw KeywIf, _) }
  else   { (TokenKeyw KeywElse, _) }
  while  { (TokenKeyw KeywWhile, _) }
  for    { (TokenKeyw KeywFor, _) }
  return { (TokenKeyw KeywReturn, _) }
  int    { (TokenKeyw KeywInt, _) }
  bool   { (TokenKeyw KeywBool, _) }
  string { (TokenKeyw KeywString, _)}
  void   { (TokenKeyw KeywVoid, _) }
  new    { (TokenKeyw KeywNew, _) }

  true  { (TokenBool Ttrue, _) }
  false { (TokenBool Tfalse, _) }

  '(' { (TokenSep SepLpar, _) }
  ')' { (TokenSep SepRpar, _) }
  '{' { (TokenSep SepLbrace, _) }
  '}' { (TokenSep SepRbrace, _) }
  '[' { (TokenSep SepLbracket, _) }
  ']' { (TokenSep SepRbracket, _) }
  ';' { (TokenSep SepSemi, _) }
  ',' { (TokenSep SepComma, _) }

  '='  { (TokenOp OpAsn, _) }
  '||' { (TokenOp OpOr, _) }
  '&&' { (TokenOp OpAnd, _) }
  '==' { (TokenOp OpEq, _) }
  '!=' { (TokenOp OpNeq, _) }
  '<'  { (TokenOp OpLt, _) }
  '>'  { (TokenOp OpGt, _) }
  '<=' { (TokenOp OpLte, _) }
  '>=' { (TokenOp OpGte, _) }
  '+'  { (TokenOp OpPlus, _) }
  '-'  { (TokenOp OpMinus, _) }
  '*'  { (TokenOp OpMul, _) }
  '/'  { (TokenOp OpDiv, _) }
  '!'  { (TokenOp OpNot, _) }
  '++' { (TokenOp OpInc, _) }
  '--' { (TokenOp OpDec, _) }

  id     { (TokenId _, _) }
  integer { (TokenInt _, _) }
  str { (TokenStr _, _) }

%right new
%right '='
%left '||'
%left '&&'
%left '==' '!='
%left '<' '<=' '>' '>='
%left '+' '-'
%left '*' '/'
%left NEG '!'

%nonassoc '['
%nonassoc '('
%nonassoc '++' '--'

%%

-- Consing instead of appending for efficiency, hence the need to reverse the
-- list afterwards.
Program : Program1 { (Program (reverse $1) :: Program Unmodified Pos) }

Program1 : {- empty -} { [] }
         | Program1 Decl { $2 : $1 }

Decl : Type id '(' FormalList ')' '{' Block '}' { Decl (posOf $2) $1 (getId $2) $4 $7 }

FormalList : {- empty -} { [] }
           | NonemptyFormalList { reverse $1 }

NonemptyFormalList : Type id { [VarDecl (posOf $2) (getId $2) $1] }
                   | NonemptyFormalList ',' Type id  { VarDecl (posOf $4) (getId $4) $3 : $1 }

Type : PrimType { Prim $1 }
     | PrimType '[' ']' { Arr $1 }

PrimType : int { Int }
         | bool { Bool }
         | void { Void }
         | string { Str }

Block : Block1 { reverse $1 }

Block1 : {- empty -} { [] }
       | Block1 BlockStmt  { $2 : $1 }

BlockStmt : Stmt { NormalStmt (stmtAnn $1) $1 }
          | Type id '=' Expr ';' { VarStmt (posOf $2) (VarDecl (posOf $2) (getId $2) $1) $4}

Stmt : '{' Block '}' { Block (posOf $1) $2 }
     | if '(' Expr ')' Stmt { If (posOf $1) $3 $5 }
     | if '(' Expr ')' Stmt else Stmt { IfElse (posOf $1) $3 $5 $7 }
     | while '(' Expr ')' Stmt { While (posOf $1) $3 $5 }
     | for '(' OptionalExpr ';' OptionalExpr ';' OptionalExpr ')' Stmt { For (posOf $1) $3 $5 $7 $9 }
     | return Expr ';' { Return (posOf $1) (Just $2) }
     | return ';' { Return (posOf $1) Nothing }
     | Expr ';' { ExprStmt (exprAnn $1) $1 }

Expr : '!' Expr { Uop (posOf $1) Not $2 }
     | '-' Expr %prec NEG { Uop (posOf $1) Negation $2 }
     | Expr '+' Expr  { Bop (exprAnn $1) $1 Add $3 }
     | Expr '-' Expr  { Bop (exprAnn $1) $1 Sub $3 }
     | Expr '*' Expr  { Bop (exprAnn $1) $1 Mul $3 }
     | Expr '/' Expr  { Bop (exprAnn $1) $1 Div $3 }
     | Expr '||' Expr { Bop (exprAnn $1) $1 Or  $3 }
     | Expr '&&' Expr { Bop (exprAnn $1) $1 And $3 }
     | Expr '==' Expr { Bop (exprAnn $1) $1 Eq  $3 }
     | Expr '!=' Expr { Bop (exprAnn $1) $1 Neq $3 }
     | Expr '<' Expr  { Bop (exprAnn $1) $1 Lt  $3 }
     | Expr '>' Expr  { Bop (exprAnn $1) $1 Gt  $3 }
     | Expr '<=' Expr { Bop (exprAnn $1) $1 Lte $3 }
     | Expr '>=' Expr { Bop (exprAnn $1) $1 Gte $3 }
     | integer { IntExpr (posOf $1) (getInt $1) }
     | str { StringExpr (posOf $1) (getStr $1) }
     | true { BoolExpr (posOf $1) True }
     | false { BoolExpr (posOf $1) False }
     | id '++' { Inc (posOf $2) (getId $1) }
     | id '--' { Dec (posOf $2) (getId $1) }
     | Expr '[' Expr ']' { Index (exprAnn $1) $1 $3 }
     | id '[' Expr ']' { Index (posOf $1) (Identifier (posOf $1) (getId $1)) $3 } -- Need this specific case to ensure indexed assignment isn't prioritized over index access
     | id { Identifier (posOf $1) (getId $1) }
     | id '(' ExprList ')' { Funcall (posOf $1) (getId $1) $3 }
     | '(' Expr ')' { $2 }
     | new PrimType '[' Expr ']' { New (posOf $1) $2 $4 }
     | Lval '=' Expr { Assignment (lvalAnn $1) $1 $3 }

ExprList : {- Empty -} { [] }
         | NonemptyExprList { reverse $1 }

NonemptyExprList : Expr { [$1] }
                 | NonemptyExprList ',' Expr { $3 : $1 }

OptionalExpr : {- empty -} { Nothing }
             | Expr { Just $1 }

Lval : id { (VarName (posOf $1) (getId $1)) }
     | id '[' Expr ']' { (IndexDest (posOf $1) (getId $1) $3) }

{
parseProgram = parse . alexScanTokens

parseError :: [(Token, Pos)] -> a
parseError [] = error "Parser error: unexpected end of stream"
parseError ((tok, (line, column)):_) =
  error $ "Parser error at line " <> show line <> ", column " <> show column
          <> ": unexpected " <> show tok

posOf = snd

getId (TokenId x, _) = x

getInt (TokenInt x, _) = read $ T.unpack x

getStr (TokenStr x, _) = x
}
