# Tiny C subset to CIL toy compiler
Toy project that compiles an imperative language with C-like syntax. Uses lexer
and parser generators and emits .NET CIL code.

## Building
The project is built with Nix in mind; theoretically it could work with cabal,
but there is no guarantee of dependency version compatibility. With Nix
installed (with flakes enabled) simply make use of the flake by running
`nix develop` in the project root, which provides `cabal` along with `mono`.
The test directory contains usable source files for use with the compiler.

## Usage
Compile a file and generate a CIL file in the current directory.
The resulting CIL file can be assembled by running `ilasm` on it,
which will generate an executable file that can be executed with `mono`.
```
cabal run toycompiler-exe -- <file>
```

Typecheck a file
```
cabal run toycompiler-exe -- -t <file>
```

Pretty print a file to standard output
```
cabal run toycompiler-exe -- -p <file>
```

Pretty print a file with renamed variables according to block scoping depth (for debug use)
```
cabal run toycompiler-exe -- -r <file>
```
